
import { NativeModules } from 'react-native';

const { RNNativeGeocoder } = NativeModules;

export default {

  geocodeAddress(address) {
    if (!address) {
      return Promise.reject(new Error("address is null"));
    }

    return RNNativeGeocoder.geocodeAddressString(address);
  }
}