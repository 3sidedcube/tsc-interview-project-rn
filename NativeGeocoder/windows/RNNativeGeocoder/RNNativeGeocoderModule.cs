using ReactNative.Bridge;
using System;
using System.Collections.Generic;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;

namespace Native.Geocoder.RNNativeGeocoder
{
    /// <summary>
    /// A module that allows JS to share data.
    /// </summary>
    class RNNativeGeocoderModule : NativeModuleBase
    {
        /// <summary>
        /// Instantiates the <see cref="RNNativeGeocoderModule"/>.
        /// </summary>
        internal RNNativeGeocoderModule()
        {

        }

        /// <summary>
        /// The name of the native module.
        /// </summary>
        public override string Name
        {
            get
            {
                return "RNNativeGeocoder";
            }
        }
    }
}
