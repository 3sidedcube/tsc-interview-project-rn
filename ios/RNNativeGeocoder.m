
#import "RNNativeGeocoder.h"

@import CoreLocation;

@implementation RNNativeGeocoder

- (dispatch_queue_t)methodQueue
{
    return dispatch_get_main_queue();
}

RCT_EXPORT_MODULE()

RCT_EXPORT_METHOD(geocodeAddressString:(NSString *)addressString
                  resolver:(RCTPromiseResolveBlock)resolve
                  rejecter:(RCTPromiseRejectBlock)reject) {
  // Calls the Swift implementation of Geocoder, to geocode the address string.
  if (self.geocoder == nil) {
    self.geocoder = [Geocoder new];
  }
  
  [self.geocoder geocodeWithAddressString:addressString resolver:resolve reject:reject];
}

@end
  
