
#if __has_include("RCTBridgeModule.h")
#import "RCTBridgeModule.h"
#else
#import <React/RCTBridgeModule.h>
#endif

#import "Geocoder-Swift.h"

@interface RNNativeGeocoder : NSObject <RCTBridgeModule>

@property (nonatomic, strong) Geocoder *geocoder;

@end
  
