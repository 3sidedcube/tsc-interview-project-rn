//
//  Geocoder.swift
//  RNNativeGeocoder
//
//  Created by Simon Mitchell on 04/02/2019.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import CoreLocation

@objc class Geocoder: NSObject {
  
  @objc func geocode(addressString: String, resolver: @escaping RCTPromiseResolveBlock, reject: @escaping RCTPromiseRejectBlock) {
    
    //TODO: Geocode the string to a list of possible coordinates and return the response
  }
}
