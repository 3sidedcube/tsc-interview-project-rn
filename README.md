# README

This project is a React Native interview task to implement a simple "**Guess the city**" game. 
It involves a small amount of native Swift integration.

We estimate that the task will take around 4 hours. 
You can use any resources to complete the task that you would use during usual development

## Project structure

The project was created using `react-native init`. 
We then added some stubbed Objective-C and Swift methods so that the Geocoding task could be completed.

You will need xcode (`v10.1`), node (`v8.12.0` and `v10.13.0` work) and react-native-cli (`2.0.1+`) installed. Then, to run:

* `yarn` or `npm install`
* `react-native run-ios` (or launch directly through xcode)

## Specification

1. On startup, the app should load the `us_cities.json` file provided in the root directory
	* One city should be randomly selected from this file. This is kept secret from the user and is the target that they must guess
1. The app should have the following UI elements
	* A text input at the top of the screen
	* A row of two buttons underneath the text input, one saying "Guess" and one saying "Ask for a clue"
	* The remainder of the screen is a list containing text output from the app - feedback to the user about their guesses. All output from the app should appear in this list in reverse chronological order (the most recent message at the top).
1. The user should be able to enter guesses into the text input
	* The "Guess" button must be disabled if the text input is empty
1. The user should be able to press the "Guess" button to submit a guess
	* In response, the app should output the number of unique letters that the guess has in common with the actual answer.
	e.g. _Incorrect. But your guess has 4 letters in common with the answer_
	* If the guess is correct then a _Success!_ message is output by the app and all further interactions are disabled.
1. The user should be able to press the "Ask for a clue" button
	* When pressed, the app should randomly select another of the cities
	* The app should geocode both this location and the secret location.
		* We would prefer that this is done by implementing some native Swift code to perform the geocode task, using [CLGeocoder](https://developer.apple.com/documentation/corelocation/clgeocoder).
		* To save time on writing bridging code, the native method required has been stubbed out for you to take a `string` argument and return a `Promise`. This can be found in the `ios/Geocoder.swift` file.
		* The geocoding should occur asynchronously without blocking the UI
		* Whilst the geocoding is ongoing UI interactions should be disabled
	* Using the geocoded co-ordinates, the app should output the relative cardinal direction of the secret city from the other randomly selected city.
		* e.g. _The target city is south-west of Detroit_
	* It is up to you how exactly the "relative location" text is computed and outputted.

## Criteria

You will be assessed on:

* Your ability to meet the specification
* Your ability to write clean, good quality, conventional code
* Your ability to employ appropriate language features and data structures
* Your ability to learn and research new solutions independently

To save time, please don't spend time on the following unless you feel it would be beneficial. 
You won't be assessed on these criteria:

* Styling of the UI
* Automated tests and other auxiliary code quality measures
* Please ignore Android support for the purposes of this task

If you get stuck or can't meet a requirement then leave a code comment explaining why, and improvise a workaround that would allow you to continue with the other tasks.

